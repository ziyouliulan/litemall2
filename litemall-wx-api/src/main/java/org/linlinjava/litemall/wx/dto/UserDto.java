package org.linlinjava.litemall.wx.dto;

import lombok.Data;

@Data
public class UserDto {

    private String avatar;
    private Byte gender;
    private Byte nickname;
}
