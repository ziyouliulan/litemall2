
package org.linlinjava.litemall.wx.web;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.CustAccountbase;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.domain.LpNotice;
import org.linlinjava.litemall.db.service.ConsumeRecordService;
import org.linlinjava.litemall.db.service.CustAccountbaseService;
import org.linlinjava.litemall.db.service.LitemallUserService;
import org.linlinjava.litemall.db.service.LpNoticeService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.linlinjava.litemall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNMATCH;


/**
* <p>说明： 用户钱包表API接口层</P>
* @version: V1.0
* @author: wlx
* @time    2021年02月19日
*
*/
@Api(value = "通知",tags="通知" )
@RestController
@RequestMapping("/wx/notice")
public class NoticeController {


	@Autowired
	LpNoticeService noticeService;

	@PostMapping("/getNotice")
	@ApiOperation(value = "修改支付密码", notes = "修改支付密码+-")
	@ApiImplicitParams({
			@ApiImplicitParam(dataType = "Integer", name = "userId",value = "用户id",required = true),
	})
	public Object upPaykey(@LoginUser Integer userId){

		LpNotice byId = noticeService.getById(1);

		return ResponseUtil.ok(byId);
	}


}
