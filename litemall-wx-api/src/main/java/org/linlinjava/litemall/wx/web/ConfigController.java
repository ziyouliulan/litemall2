
package org.linlinjava.litemall.wx.web;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.checkerframework.checker.units.qual.A;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.CustAccountbase;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.service.ClientConfigService;
import org.linlinjava.litemall.db.service.ConsumeRecordService;
import org.linlinjava.litemall.db.service.CustAccountbaseService;
import org.linlinjava.litemall.db.service.LitemallUserService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

import static org.linlinjava.litemall.wx.util.WxResponseCode.AUTH_CAPTCHA_UNMATCH;


/**
* <p>说明： 用户钱包表API接口层</P>
* @version: V1.0
* @author: wlx
* @time    2021年02月19日
*
*/
@Api(value = "app配置",tags="app配置" )
@RestController
@RequestMapping("/wx/config")
public class ConfigController {


	@Autowired
	ClientConfigService clientConfigService;

	@PostMapping("/info")
	@ApiOperation(value = "修改支付密码", notes = "修改支付密码+-")
	@ApiImplicitParams({
			@ApiImplicitParam(dataType = "Integer", name = "userId",value = "用户id",required = true),
	})
	public Object upPaykey(@LoginUser Integer userId){
		JSONObject jsonObject = clientConfigService.configToMap();

		return ResponseUtil.ok(jsonObject);
	}



}
