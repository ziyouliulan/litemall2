# From java image, version : 8
FROM java:8

# 挂载app目录
VOLUME /app

# COPY or ADD to image
COPY litemall-all/target/litemall-all-0.1.0-exec.jar app.jar

COPY docker/application.yml  application.yml
COPY docker/application-wx.yml  application-wx.yml
COPY docker/application-db.yml  application-db.yml
COPY docker/application-core.yml  application-core.yml
COPY docker/application-admin.yml  application-admin.yml

RUN bash -c "touch /app.jar"
EXPOSE 8080
#ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=86", "-jar", "app.jar","--spring.config.location=application.yml,application-druid.yml"]
#ENTRYPOINT ["java", "-jar", "app.jar","--spring.config.location=application.yml"]
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=86","-jar", "app.jar","--spring.config.location=application.yml,application-db.yml,application-wx.yml,application-core.yml,application-admin.yml"]
