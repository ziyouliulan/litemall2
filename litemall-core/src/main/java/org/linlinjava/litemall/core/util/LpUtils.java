package org.linlinjava.litemall.core.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LpUtils {

    public  static String generateOrderNumber(){

        LocalDateTime time = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String format = dateTimeFormatter.format(time);
        String numbers = RandomStringUtils.randomNumeric(5);
        String liushui = format+numbers;
        return  liushui;

    }
}
