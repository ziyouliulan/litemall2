package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.CustAccountbase;
import org.linlinjava.litemall.db.service.CustAccountbaseService;
import org.linlinjava.litemall.db.dao.CustAccountbaseMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【cust_accountbase(用户钱包表)】的数据库操作Service实现
* @createDate 2022-05-03 21:21:29
*/
@Service
public class CustAccountbaseServiceImpl extends ServiceImpl<CustAccountbaseMapper, CustAccountbase>
    implements CustAccountbaseService{

    @Override
    public CustAccountbase getCustAccountbase(Integer userId) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_id",userId);
        CustAccountbase one = getOne(wrapper);
        return one;
    }
}




