package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.ClientConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【client_config(参数配置表)】的数据库操作Mapper
* @createDate 2022-05-03 22:47:38
* @Entity org.linlinjava.litemall.db.domain.ClientConfig
*/
public interface ClientConfigMapper extends BaseMapper<ClientConfig> {

}




