package org.linlinjava.litemall.db.service;

import com.alibaba.fastjson.JSONObject;
import org.linlinjava.litemall.db.domain.ClientConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
* @author lpden
* @description 针对表【client_config(参数配置表)】的数据库操作Service
* @createDate 2022-05-03 22:47:38
*/
public interface ClientConfigService extends IService<ClientConfig> {

    JSONObject configToMap();

}
