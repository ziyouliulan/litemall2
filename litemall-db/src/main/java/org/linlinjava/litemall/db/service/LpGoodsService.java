package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.LpGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【lp_goods(商品基本信息表)】的数据库操作Service
* @createDate 2022-05-03 23:19:26
*/
public interface LpGoodsService extends IService<LpGoods> {

}
