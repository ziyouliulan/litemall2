package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.LpGoods;
import org.linlinjava.litemall.db.service.LpGoodsService;
import org.linlinjava.litemall.db.dao.LpGoodsMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【lp_goods(商品基本信息表)】的数据库操作Service实现
* @createDate 2022-05-03 23:19:26
*/
@Service
public class LpGoodsServiceImpl extends ServiceImpl<LpGoodsMapper, LpGoods>
    implements LpGoodsService{

}




