package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.LpMarket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【lp_market】的数据库操作Service
* @createDate 2022-05-08 20:20:52
*/
public interface LpMarketService extends IService<LpMarket> {

}
