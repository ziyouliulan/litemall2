package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.LpMarket;
import org.linlinjava.litemall.db.service.LpMarketService;
import org.linlinjava.litemall.db.dao.LpMarketMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【lp_market】的数据库操作Service实现
* @createDate 2022-05-08 20:20:52
*/
@Service
public class LpMarketServiceImpl extends ServiceImpl<LpMarketMapper, LpMarket>
    implements LpMarketService{

}




