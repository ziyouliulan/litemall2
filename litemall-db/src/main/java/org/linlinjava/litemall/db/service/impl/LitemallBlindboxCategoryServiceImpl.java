package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.LitemallBlindboxCategory;
import org.linlinjava.litemall.db.service.LitemallBlindboxCategoryService;
import org.linlinjava.litemall.db.dao.LitemallBlindboxCategoryMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【litemall_blindbox_category(类目表)】的数据库操作Service实现
* @createDate 2022-05-26 21:31:32
*/
@Service
public class LitemallBlindboxCategoryServiceImpl extends ServiceImpl<LitemallBlindboxCategoryMapper, LitemallBlindboxCategory>
    implements LitemallBlindboxCategoryService{

}




