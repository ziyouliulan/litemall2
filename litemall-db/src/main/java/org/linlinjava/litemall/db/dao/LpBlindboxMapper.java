package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LpBlindbox;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【lp_blindbox(商品基本信息表)】的数据库操作Mapper
* @createDate 2022-05-05 20:12:44
* @Entity org.linlinjava.litemall.db.domain.LpBlindbox
*/
public interface LpBlindboxMapper extends BaseMapper<LpBlindbox> {

}




