package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LitemallBlindboxCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【litemall_blindbox_category(类目表)】的数据库操作Mapper
* @createDate 2022-05-26 21:31:32
* @Entity org.linlinjava.litemall.db.domain.LitemallBlindboxCategory
*/
public interface LitemallBlindboxCategoryMapper extends BaseMapper<LitemallBlindboxCategory> {

}




