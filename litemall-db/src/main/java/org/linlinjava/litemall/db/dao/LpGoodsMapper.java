package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LpGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【lp_goods(商品基本信息表)】的数据库操作Mapper
* @createDate 2022-05-03 23:19:26
* @Entity org.linlinjava.litemall.db.domain.LpGoods
*/
public interface LpGoodsMapper extends BaseMapper<LpGoods> {

}




