package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.LpBlindbox;
import org.linlinjava.litemall.db.service.LpBlindboxService;
import org.linlinjava.litemall.db.dao.LpBlindboxMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【lp_blindbox(商品基本信息表)】的数据库操作Service实现
* @createDate 2022-05-05 20:12:44
*/
@Service
public class LpBlindboxServiceImpl extends ServiceImpl<LpBlindboxMapper, LpBlindbox>
    implements LpBlindboxService{

}




