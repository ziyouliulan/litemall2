package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.LpGoodsBlindbox;
import org.linlinjava.litemall.db.service.LpGoodsBlindboxService;
import org.linlinjava.litemall.db.dao.LpGoodsBlindboxMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【lp_goods_blindbox(商品基本信息表)】的数据库操作Service实现
* @createDate 2022-05-04 17:57:55
*/
@Service
public class LpGoodsBlindboxServiceImpl extends ServiceImpl<LpGoodsBlindboxMapper, LpGoodsBlindbox>
    implements LpGoodsBlindboxService{

}




