package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.LpNotice;
import org.linlinjava.litemall.db.service.LpNoticeService;
import org.linlinjava.litemall.db.dao.LpNoticeMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【lp_notice】的数据库操作Service实现
* @createDate 2022-05-03 21:56:15
*/
@Service
public class LpNoticeServiceImpl extends ServiceImpl<LpNoticeMapper, LpNotice>
    implements LpNoticeService{

}




