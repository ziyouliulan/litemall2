package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.CustAccountbase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【cust_accountbase(用户钱包表)】的数据库操作Mapper
* @createDate 2022-05-03 21:21:29
* @Entity org.linlinjava.litemall.db.domain.CustAccountbase
*/
public interface CustAccountbaseMapper extends BaseMapper<CustAccountbase> {

}




