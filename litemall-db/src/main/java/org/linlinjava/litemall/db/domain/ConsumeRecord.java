package org.linlinjava.litemall.db.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 消费记录
 * @TableName consume_record
 */
@TableName(value ="consume_record")
@Data
public class ConsumeRecord implements Serializable {
    /**
     * 类型：
1:用户充值, 2:用户提现, 3:后台充值, 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 交易单号
     */
    private String tradeNo;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     *  对方用户Id：
     */
    private Long toUserId;

    /**
     * 金额
     */
    private Double money;

    /**
     * 类型：
1:用户充值, 2:用户提现, 3:后台充值, 4购买商品 5出售商品 
     */
    private Integer type;

    /**
     * 消费备注
     */
    private String descs;

    /**
     *  1：支付宝支付 , 2：微信支付, 3：余额支付, 4:系统支付，5，推荐钱包  6商家钱包
     */
    private Integer payType;

    /**
     * 类型：
1:用户充值, 2:用户提现, 3:后台充值,
     */
    private Integer status;

    /**
     * 类型：
1:用户充值, 2:用户提现, 3:后台充值, 
     */
    private String redPacketId;

    /**
     *  1 收入  2支出
     */
    private Integer changeType;

    /**
     * 超过一定金额需要后台审核：1.审核成功 ，-1.审核失败
     */
    private Integer manualPayStatus;

    /**
     * 手续费
     */
    private Double serviceCharge;

    /**
     * 当前余额
     */
    private Double currentBalance;

    /**
     * 实际操作金额
     */
    private Double operationAmount;

    /**
     * 时间
     */
    private Date time;

    /**
     * 到账时间
     */
    private Date timestamp;

    /**
     * 商品购买优惠
     */
    private Double copyrightFee;

    /**
     * 积分获得率
     */
    private Double payProcessingFees;

    /**
     * 推荐抽成
     */
    private Double platformFee;

    /**
     * 微信支付单号
     */
    private String transactionId;

    /**
     * 商户订单号
     */
    private String outTradeNo;

    /**
     * 
     */
    private String bankCardId;

    /**
     * 测试
     */
    private String ncountOrderId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ConsumeRecord other = (ConsumeRecord) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTradeNo() == null ? other.getTradeNo() == null : this.getTradeNo().equals(other.getTradeNo()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getToUserId() == null ? other.getToUserId() == null : this.getToUserId().equals(other.getToUserId()))
            && (this.getMoney() == null ? other.getMoney() == null : this.getMoney().equals(other.getMoney()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getDescs() == null ? other.getDescs() == null : this.getDescs().equals(other.getDescs()))
            && (this.getPayType() == null ? other.getPayType() == null : this.getPayType().equals(other.getPayType()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getRedPacketId() == null ? other.getRedPacketId() == null : this.getRedPacketId().equals(other.getRedPacketId()))
            && (this.getChangeType() == null ? other.getChangeType() == null : this.getChangeType().equals(other.getChangeType()))
            && (this.getManualPayStatus() == null ? other.getManualPayStatus() == null : this.getManualPayStatus().equals(other.getManualPayStatus()))
            && (this.getServiceCharge() == null ? other.getServiceCharge() == null : this.getServiceCharge().equals(other.getServiceCharge()))
            && (this.getCurrentBalance() == null ? other.getCurrentBalance() == null : this.getCurrentBalance().equals(other.getCurrentBalance()))
            && (this.getOperationAmount() == null ? other.getOperationAmount() == null : this.getOperationAmount().equals(other.getOperationAmount()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
            && (this.getTimestamp() == null ? other.getTimestamp() == null : this.getTimestamp().equals(other.getTimestamp()))
            && (this.getCopyrightFee() == null ? other.getCopyrightFee() == null : this.getCopyrightFee().equals(other.getCopyrightFee()))
            && (this.getPayProcessingFees() == null ? other.getPayProcessingFees() == null : this.getPayProcessingFees().equals(other.getPayProcessingFees()))
            && (this.getPlatformFee() == null ? other.getPlatformFee() == null : this.getPlatformFee().equals(other.getPlatformFee()))
            && (this.getTransactionId() == null ? other.getTransactionId() == null : this.getTransactionId().equals(other.getTransactionId()))
            && (this.getOutTradeNo() == null ? other.getOutTradeNo() == null : this.getOutTradeNo().equals(other.getOutTradeNo()))
            && (this.getBankCardId() == null ? other.getBankCardId() == null : this.getBankCardId().equals(other.getBankCardId()))
            && (this.getNcountOrderId() == null ? other.getNcountOrderId() == null : this.getNcountOrderId().equals(other.getNcountOrderId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTradeNo() == null) ? 0 : getTradeNo().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getToUserId() == null) ? 0 : getToUserId().hashCode());
        result = prime * result + ((getMoney() == null) ? 0 : getMoney().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getDescs() == null) ? 0 : getDescs().hashCode());
        result = prime * result + ((getPayType() == null) ? 0 : getPayType().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getRedPacketId() == null) ? 0 : getRedPacketId().hashCode());
        result = prime * result + ((getChangeType() == null) ? 0 : getChangeType().hashCode());
        result = prime * result + ((getManualPayStatus() == null) ? 0 : getManualPayStatus().hashCode());
        result = prime * result + ((getServiceCharge() == null) ? 0 : getServiceCharge().hashCode());
        result = prime * result + ((getCurrentBalance() == null) ? 0 : getCurrentBalance().hashCode());
        result = prime * result + ((getOperationAmount() == null) ? 0 : getOperationAmount().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getTimestamp() == null) ? 0 : getTimestamp().hashCode());
        result = prime * result + ((getCopyrightFee() == null) ? 0 : getCopyrightFee().hashCode());
        result = prime * result + ((getPayProcessingFees() == null) ? 0 : getPayProcessingFees().hashCode());
        result = prime * result + ((getPlatformFee() == null) ? 0 : getPlatformFee().hashCode());
        result = prime * result + ((getTransactionId() == null) ? 0 : getTransactionId().hashCode());
        result = prime * result + ((getOutTradeNo() == null) ? 0 : getOutTradeNo().hashCode());
        result = prime * result + ((getBankCardId() == null) ? 0 : getBankCardId().hashCode());
        result = prime * result + ((getNcountOrderId() == null) ? 0 : getNcountOrderId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", tradeNo=").append(tradeNo);
        sb.append(", userId=").append(userId);
        sb.append(", toUserId=").append(toUserId);
        sb.append(", money=").append(money);
        sb.append(", type=").append(type);
        sb.append(", descs=").append(descs);
        sb.append(", payType=").append(payType);
        sb.append(", status=").append(status);
        sb.append(", redPacketId=").append(redPacketId);
        sb.append(", changeType=").append(changeType);
        sb.append(", manualPayStatus=").append(manualPayStatus);
        sb.append(", serviceCharge=").append(serviceCharge);
        sb.append(", currentBalance=").append(currentBalance);
        sb.append(", operationAmount=").append(operationAmount);
        sb.append(", time=").append(time);
        sb.append(", timestamp=").append(timestamp);
        sb.append(", copyrightFee=").append(copyrightFee);
        sb.append(", payProcessingFees=").append(payProcessingFees);
        sb.append(", platformFee=").append(platformFee);
        sb.append(", transactionId=").append(transactionId);
        sb.append(", outTradeNo=").append(outTradeNo);
        sb.append(", bankCardId=").append(bankCardId);
        sb.append(", ncountOrderId=").append(ncountOrderId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}