package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LpGoodsBlindbox;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【lp_goods_blindbox(商品基本信息表)】的数据库操作Mapper
* @createDate 2022-05-04 17:57:55
* @Entity org.linlinjava.litemall.db.domain.LpGoodsBlindbox
*/
public interface LpGoodsBlindboxMapper extends BaseMapper<LpGoodsBlindbox> {

}




