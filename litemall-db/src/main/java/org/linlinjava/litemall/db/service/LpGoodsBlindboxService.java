package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.LpGoodsBlindbox;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【lp_goods_blindbox(商品基本信息表)】的数据库操作Service
* @createDate 2022-05-04 17:57:55
*/
public interface LpGoodsBlindboxService extends IService<LpGoodsBlindbox> {

}
