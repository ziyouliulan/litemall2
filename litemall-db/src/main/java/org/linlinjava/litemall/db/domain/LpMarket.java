package org.linlinjava.litemall.db.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName lp_market
 */
@TableName(value ="lp_market")
@Data
public class LpMarket implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 藏品id 
     */
    private Integer goodsId;

    /**
     * 1：藏品 2盲盒
     */
    private Integer type;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 盲盒id 或者藏品id
     */
    private Integer goodsBlindbox;

    /**
     * 版权费
     */
    private Double copyrightFee;

    /**
     * 支付手续费
     */
    private Double payProcessingFees;

    /**
     * 平台手续费
     */
    private Double platformFee;

    /**
     * 
     */
    private Double money;

    /**
     *
     */
    private Double actualMoney;


    @TableField(exist = false)
    private LpGoods lpGoods;

    @TableField(exist = false)
    private LpGoodsBlindbox lpGoodsBlindbox;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LpMarket other = (LpMarket) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGoodsId() == null ? other.getGoodsId() == null : this.getGoodsId().equals(other.getGoodsId()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getGoodsBlindbox() == null ? other.getGoodsBlindbox() == null : this.getGoodsBlindbox().equals(other.getGoodsBlindbox()))
            && (this.getCopyrightFee() == null ? other.getCopyrightFee() == null : this.getCopyrightFee().equals(other.getCopyrightFee()))
            && (this.getPayProcessingFees() == null ? other.getPayProcessingFees() == null : this.getPayProcessingFees().equals(other.getPayProcessingFees()))
            && (this.getPlatformFee() == null ? other.getPlatformFee() == null : this.getPlatformFee().equals(other.getPlatformFee()))
            && (this.getMoney() == null ? other.getMoney() == null : this.getMoney().equals(other.getMoney()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGoodsId() == null) ? 0 : getGoodsId().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getGoodsBlindbox() == null) ? 0 : getGoodsBlindbox().hashCode());
        result = prime * result + ((getCopyrightFee() == null) ? 0 : getCopyrightFee().hashCode());
        result = prime * result + ((getPayProcessingFees() == null) ? 0 : getPayProcessingFees().hashCode());
        result = prime * result + ((getPlatformFee() == null) ? 0 : getPlatformFee().hashCode());
        result = prime * result + ((getMoney() == null) ? 0 : getMoney().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", goodsId=").append(goodsId);
        sb.append(", type=").append(type);
        sb.append(", userId=").append(userId);
        sb.append(", goodsBlindbox=").append(goodsBlindbox);
        sb.append(", copyrightFee=").append(copyrightFee);
        sb.append(", payProcessingFees=").append(payProcessingFees);
        sb.append(", platformFee=").append(platformFee);
        sb.append(", money=").append(money);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}