package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.UserGoods;
import org.linlinjava.litemall.db.service.UserGoodsService;
import org.linlinjava.litemall.db.dao.UserGoodsMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【user_goods】的数据库操作Service实现
* @createDate 2022-05-04 17:57:18
*/
@Service
public class UserGoodsServiceImpl extends ServiceImpl<UserGoodsMapper, UserGoods>
    implements UserGoodsService{

}




