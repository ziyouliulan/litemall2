package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.UserGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【user_goods】的数据库操作Mapper
* @createDate 2022-05-04 17:57:18
* @Entity org.linlinjava.litemall.db.domain.UserGoods
*/
public interface UserGoodsMapper extends BaseMapper<UserGoods> {

}




