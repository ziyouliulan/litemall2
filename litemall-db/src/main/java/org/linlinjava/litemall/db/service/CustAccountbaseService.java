package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.CustAccountbase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【cust_accountbase(用户钱包表)】的数据库操作Service
* @createDate 2022-05-03 21:21:29
*/
public interface CustAccountbaseService extends IService<CustAccountbase> {


    CustAccountbase getCustAccountbase(Integer userId);
}
