package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.ConsumeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【consume_record(消费记录)】的数据库操作Mapper
* @createDate 2022-05-03 19:23:06
* @Entity org.linlinjava.litemall.db.domain.ConsumeRecord
*/
public interface ConsumeRecordMapper extends BaseMapper<ConsumeRecord> {

}




