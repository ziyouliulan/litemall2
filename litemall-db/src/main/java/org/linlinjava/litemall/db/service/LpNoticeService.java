package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.LpNotice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【lp_notice】的数据库操作Service
* @createDate 2022-05-03 21:56:15
*/
public interface LpNoticeService extends IService<LpNotice> {

}
