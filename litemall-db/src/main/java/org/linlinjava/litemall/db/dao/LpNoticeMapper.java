package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LpNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【lp_notice】的数据库操作Mapper
* @createDate 2022-05-03 21:56:15
* @Entity org.linlinjava.litemall.db.domain.LpNotice
*/
public interface LpNoticeMapper extends BaseMapper<LpNotice> {

}




