package org.linlinjava.litemall.db.dao;

import org.linlinjava.litemall.db.domain.LpMarket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lpden
* @description 针对表【lp_market】的数据库操作Mapper
* @createDate 2022-05-08 20:20:52
* @Entity org.linlinjava.litemall.db.domain.LpMarket
*/
public interface LpMarketMapper extends BaseMapper<LpMarket> {

}




