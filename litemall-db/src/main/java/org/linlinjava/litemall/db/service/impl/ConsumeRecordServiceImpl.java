package org.linlinjava.litemall.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.ConsumeRecord;
import org.linlinjava.litemall.db.service.ConsumeRecordService;
import org.linlinjava.litemall.db.dao.ConsumeRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author lpden
* @description 针对表【consume_record(消费记录)】的数据库操作Service实现
* @createDate 2022-05-03 19:23:06
*/
@Service
public class ConsumeRecordServiceImpl extends ServiceImpl<ConsumeRecordMapper, ConsumeRecord>
    implements ConsumeRecordService{

}




