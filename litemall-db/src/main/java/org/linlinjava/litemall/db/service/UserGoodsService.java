package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.UserGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【user_goods】的数据库操作Service
* @createDate 2022-05-04 17:57:18
*/
public interface UserGoodsService extends IService<UserGoods> {

}
