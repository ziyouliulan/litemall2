package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.domain.ConsumeRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lpden
* @description 针对表【consume_record(消费记录)】的数据库操作Service
* @createDate 2022-05-03 19:23:06
*/
public interface ConsumeRecordService extends IService<ConsumeRecord> {

}
