package org.linlinjava.litemall.db.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.linlinjava.litemall.db.domain.ClientConfig;
import org.linlinjava.litemall.db.service.ClientConfigService;
import org.linlinjava.litemall.db.dao.ClientConfigMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
* @author lpden
* @description 针对表【client_config(参数配置表)】的数据库操作Service实现
* @createDate 2022-05-03 22:47:38
*/
@Service
public class ClientConfigServiceImpl extends ServiceImpl<ClientConfigMapper, ClientConfig>
    implements ClientConfigService{

    @Override
    public JSONObject configToMap() {
        List<ClientConfig> list = list(null);
        HashMap map = new HashMap();
        for (ClientConfig clientConfig: list
        ) {

            map.put(clientConfig.getConfigKey(),clientConfig.getConfigValue());

        }
        String s = JSONObject.toJSONString(map);
        JSONObject jsonObject = JSONObject.parseObject(s);
        return jsonObject;
    }


}




